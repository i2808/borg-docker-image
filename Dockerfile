FROM debian



RUN apt-get update && \
  apt-get install -y libc-bin && \
  apt-get update && \
  apt-get install -y --no-install-recommends openssh-client mariadb-client borgbackup python3-pip || true && \
  dpkg --purge --force-all libc-bin && \
  apt-get install -y --no-install-recommends openssh-client mariadb-client borgbackup python3-pip && \
  rm -rf /var/lib/apt/lists/*


RUN pip3 install --user --upgrade borgmatic
RUN echo export 'PATH="$PATH:/root/.local/bin"' >> ~/.bashrc
RUN . ~/.bashrc

COPY known_hosts /root/.ssh/known_hosts
